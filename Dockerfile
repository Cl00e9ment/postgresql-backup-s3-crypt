FROM alpine:3

# add dependencies
RUN apk add aws-cli bash coreutils gnupg postgresql xz

# setup user
RUN adduser -D backuper
WORKDIR /home/backuper

# add scripts
COPY scripts/backup scripts/fetch scripts/list /usr/local/bin/
COPY scripts/cron.sh .
RUN chmod 755 \
	/usr/local/bin/backup \
	/usr/local/bin/fetch \
	/usr/local/bin/list \
	/home/backuper/cron.sh
RUN chown backuper:backuper cron.sh

CMD ./cron.sh
